<?php

declare(strict_types=1);

namespace Dasoe\Bergbundsite\Domain\Repository;


/**
 * This file is part of the "xugeos" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 
 */

/**
 * The repository for Xugeos
 */
class IconRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
