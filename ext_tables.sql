
CREATE TABLE tt_content (
  tx_bergbundsite_color varchar(255) DEFAULT '' NOT NULL,
  tx_bergbundsite_icon int(11) unsigned NOT NULL default '0',
);

CREATE TABLE tx_bergbundsite_icon_domain_model_icon (
    title varchar(255) DEFAULT '' NOT NULL,
    image int(11) unsigned NOT NULL default '0',
);

