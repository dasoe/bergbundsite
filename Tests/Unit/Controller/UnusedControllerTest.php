<?php
declare(strict_types=1);

namespace Dasoe\Bergbundsite\Tests\Unit\Controller;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class UnusedControllerTest extends UnitTestCase
{
    /**
     * @var \Dasoe\Bergbundsite\Controller\UnusedController
     */
    protected $subject;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Dasoe\Bergbundsite\Controller\UnusedController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllUnusedsFromRepositoryAndAssignsThemToView()
    {
        $allUnuseds = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $unusedRepository = $this->getMockBuilder(\Dasoe\Bergbundsite\Domain\Repository\UnusedRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $unusedRepository->expects(self::once())->method('findAll')->will(self::returnValue($allUnuseds));
        $this->inject($this->subject, 'unusedRepository', $unusedRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('unuseds', $allUnuseds);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
