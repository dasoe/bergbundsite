<?php
declare(strict_types=1);

namespace Dasoe\Bergbundsite\Tests\Unit\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class UnusedTest extends UnitTestCase
{
    /**
     * @var \Dasoe\Bergbundsite\Domain\Model\Unused
     */
    protected $subject;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Dasoe\Bergbundsite\Domain\Model\Unused();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
