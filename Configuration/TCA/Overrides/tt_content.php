<?php
defined('TYPO3_MODE') || die();

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
   'tt_content',
   'CType',
    [
        'LLL:EXT:bergbundsite/Resources/Private/Language/Tca.xlf:bergbundsite_teaserbullet',
        'bergbundsite_teaserbullet',
        'bergbundsite-teaserbullet',
    ],
    'textmedia',
    'after'
);

$temporaryColumn = [
   'tx_bergbundsite_color' => [
      'exclude' => 0,
      'label' => 'LLL:EXT:bergbundsite/Resources/Private/Language/locallang_db.xlf:tt_content.tx_bergbundsite_color',
      'config' => [
         'type' => 'select',
         'renderType' => 'selectSingle',
         'items' => [
            ['Dunkles Blau', '004575'],
            ['Halles Blau', '0071bb'],
            ['Knallgrün', '4dbd38'],
         ],
         'default' => '004575',
      ],
   ],
   'tx_bergbundsite_icon' => [
      'exclude' => 0,
      'label' => 'LLL:EXT:bergbundsite/Resources/Private/Language/locallang_db.xlf:tt_content.tx_bergbundsite_icon',
      'config' => [
         'type' => 'select',
         'renderType' => 'selectSingle',
        'default' => 0,
        'items' => [
            ['-- bitte wählen --', 0],
        ],
        'foreign_table' => 'tx_bergbundsite_icon_domain_model_icon',
      ],
   ],
    
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $temporaryColumn);




// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['bergbundsite_teaserbullet'] = [
    'showitem' => '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;;general,
            --palette--;;headers,
            tx_bergbundsite_color,
            tx_bergbundsite_icon,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;;frames,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
    ',
    'columnsOverrides' => [
//        'bodytext' => [
//            'config' => [
//                'enableRichtext' => true,
//                'richtextConfiguration' => 'default',
//            ],
//        ],
    ],
];