<?php

defined('TYPO3_MODE') || die();

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            'bergbundsite',
            'Configuration/TSconfig/Page/BackendLayouts.tsconfig',
            'Backend Layouts Bergbund'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            'bergbundsite',
            'Configuration/TSconfig/Page/ContentElements.tsconfig',
            'Content Elements Bergbund'
    );    