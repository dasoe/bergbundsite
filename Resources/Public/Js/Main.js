/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    console.log("ready!");
    updateUnits();
    
    $('#mainmenu .menuLevel1 li').not(".menuLevel2 li").on("mouseover", function (event, data) {
        if (!$(this).hasClass("open")) {
            openSubMenu($(this));
        }
    });
    $('#menuCloseOverlay').on("mouseover", function (event, data) {
        closeAllSubMenus();
        $(this).css('display', 'none');
    });
    
});



function updateUnits() {
    // columns in Content are calculated by flex
    // get size of one Unity ( one quad in grid )
    // using columns calculated by flex as a basis

//    console.log($(window).innerWidth());
//    console.log(1600 / ($(window).innerWidth() * 5));


    $('#oeVars').html($(window).innerWidth());
    if ($(window).innerWidth() > 1024) {
        // Desktop
//        oUnt = $('#wrap').width() / 48;
        oPx = ($(window).innerWidth() / 1600);
        +(1600 / ($(window).innerWidth() * 10));
        if (oPx > 1) {
            oPx = 1;
        }
        env = "desktop";
    } else if ($(window).innerWidth() > 440 && $(window).innerWidth() < 1025) {
        // Tablet
//        oUnt = $('#wrap').width() / 25;
        oPx = ($(window).innerWidth() / 1025);
        +(1025 / ($(window).innerWidth() * 10));
        if (oPx > 1) {
            oPx = 1;
        }
        env = "tablet";
    } else {
        // Mobile
//        oUnt = $('#wrap').width() / 9;
        env = "mobile";
    }
    
    console.log(env);

//    console.log('oUnt after updating: ' + oUnt + ' - oPx after updating: ' + oPx);

}




function openMobileMenu() {
    console.log("open mobile Menu");
    $('#mobilemenuContent').velocity({
        height: $('#mobilemenuContent').data('originalHeight'),
    }, 650, function () {
        $('#mobilemenuContent').css('height', 'auto');
    });


}

function closeMobileMenu() {
    console.log("close mobile Menu");
    $('#mobilemenuContent').velocity({
        height: 0,
    }, 650, function () {
    });

}

function openMobileSubMenu(element) {
    console.log("open mobile SUBMenu");
    element.velocity({
        height: element.data('originalHeight'),
    }, 650, function () {
        element.css('height', 'auto');
    });

}


function closeMobileSubMenu(element) {
    console.log("close mobile SUBMenu");
    element.velocity({
        height: 0,
    }, 650, function () {
    });

}

function openSubMenu(element) {
    closeAllSubMenus();
    console.log("open sub menu");
    element.addClass("open");
    element.find(".menuLevel2").addClass("open");
    $('#menuCloseOverlay').css("display", 'block');
}

function closeAllSubMenus() {
    console.log("close all sub menus");
    $(".menuLevel2").removeClass("open");
    $(".menuLevel1 li").removeClass("open");
}
